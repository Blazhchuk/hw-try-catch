const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.getElementById('root');

const list = document.createElement('ul');

books.forEach(book => {
    try {
        if (book.author && book.name && book.price) {
            const listItem = document.createElement('li');
            listItem.textContent = `${book.author} - ${book.name} (${book.price} грн.)`;

            list.appendChild(listItem);
        } else {
            throw new Error(`Необхідні властивості відсутні в об'єкті: ${!book.author ? 'author' : ''}${!book.name ? 'name' : ''}${!book.price ? 'price' : ''}`);
        }
    } catch (error) {
        console.error(error.message);
    }
});

root.appendChild(list);
